class CreatePortfolios < ActiveRecord::Migration
  def change
    create_table :portfolios do |t|
      t.string 	:ticker
      t.integer :quantity

      t.timestamps
    end
  end
end
