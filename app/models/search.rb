class Search < ActiveRecord::Base
	attr_accessible :ticker

	validates :ticker, presence: true, :on => [:create]

	@search = Search.new
end
