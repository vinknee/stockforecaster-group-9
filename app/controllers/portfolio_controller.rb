class PortfolioController < ApplicationController

  	def index
  		@portfolio = Portfolio.all
  		@new_portfolio = Portfolio.new
  		render "index"
  	end

  	def new
  		@portfolio = Portfolio.new
  		@portfolio.save

  	end

  	def add
  		@portfolio = Portfolio.create(:ticker => params[:portfolio][:ticker], :quantity => params[:portfolio][:quantity]);

  		redirect_to :action => 'index'
  	end

  	def remove
  	end
end
