// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(function() {

    var symbol = "AAPL";
    var Query = "http://www.quandl.com/api/v1/datasets/WIKI/"+symbol+".json?sort_order=asc";
    console.log(Query);
    //get date from 
    $.getJSON(Query, function(data,x,y) {

        //var date=data['query']['results']['quote']['Date'];
        var quoteArray = data['data'];
        console.log(quoteArray);
        var theLength = quoteArray.length; //get size of array

        var date = [];

        //convert date to milliseconds
        for (i=0; i< theLength; i++) {
            var myDate = new Date(quoteArray[i][0]);
            var dateyo = myDate.getTime(); //date in milliseconds to plot correctly with highCharts
            date.push([
                dateyo
            ]);
        }
        console.log(date[1][0]);
        //populate arrays for highchart
        var ohlc = [],
            volume = [];
        for (i=0; i<theLength; i++) {
            ohlc.push([
                date[i][0], //the date
                quoteArray[i][1], //open
                quoteArray[i][2], //high
                quoteArray[i][3], //low
                quoteArray[i][4] //close
            ]);

            volume.push([
                date[i][0], //date
                quoteArray[i][5] //Volume
            ])
        }

            // set the allowed units for data grouping
        var groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]];

        // create the chart
        $('#container').highcharts('StockChart', {
            
            rangeSelector: {
                selected: 1
            },

            title: {
                text: ''+symbol+' Historical'
            },

            yAxis: [{
                title: {
                    text: 'OHLC'
                },
                height: 200,
                lineWidth: 2
            }, {
                title: {
                    text: 'Volume'
                },
                top: 300,
                height: 100,
                offset: 0,
                lineWidth: 2
            }],
            
            series: [{
                type: 'candlestick',
                name: symbol,
                data: ohlc,
                dataGrouping: {
                    units: groupingUnits
                }
            }, {
                type: 'column',
                name: 'Volume',
                data: volume,
                yAxis: 1,
                dataGrouping: {
                    units: groupingUnits
                }
            }]
        });
    });
        var symbol2 = "GOOG";
    var Query2 = "http://www.quandl.com/api/v1/datasets/WIKI/"+symbol2+".json?sort_order=asc";
    console.log(Query2);
    //get date from 
    $.getJSON(Query2, function(data,x,y) {

        //var date=data['query']['results']['quote']['Date'];
        var quoteArray = data['data'];
        console.log(quoteArray);
        var theLength = quoteArray.length; //get size of array

        var date = [];

        //convert date to milliseconds
        for (i=0; i< theLength; i++) {
            var myDate = new Date(quoteArray[i][0]);
            var dateyo = myDate.getTime(); //date in milliseconds to plot correctly with highCharts
            date.push([
                dateyo
            ]);
        }
        console.log(date[1][0]);
        //populate arrays for highchart
        var ohlc = [],
            volume = [];
        for (i=0; i<theLength; i++) {
            ohlc.push([
                date[i][0], //the date
                quoteArray[i][1], //open
                quoteArray[i][2], //high
                quoteArray[i][3], //low
                quoteArray[i][4] //close
            ]);

            volume.push([
                date[i][0], //date
                quoteArray[i][5] //Volume
            ])
        }

            // set the allowed units for data grouping
        var groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]];

        // create the chart
        $('#container2').highcharts('StockChart', {
            
            rangeSelector: {
                selected: 1
            },

            title: {
                text: ''+symbol2+' Historical'
            },

            yAxis: [{
                title: {
                    text: 'OHLC'
                },
                height: 200,
                lineWidth: 2
            }, {
                title: {
                    text: 'Volume'
                },
                top: 300,
                height: 100,
                offset: 0,
                lineWidth: 2
            }],
            
            series: [{
                type: 'candlestick',
                name: symbol2,
                data: ohlc,
                dataGrouping: {
                    units: groupingUnits
                }
            }, {
                type: 'column',
                name: 'Volume',
                data: volume,
                yAxis: 1,
                dataGrouping: {
                    units: groupingUnits
                }
            }]
        });
    });
        var symbol3 = "FB";
    var Query3 = "http://www.quandl.com/api/v1/datasets/WIKI/"+symbol3+".json?sort_order=asc";
    console.log(Query);
    //get date from 
    $.getJSON(Query3, function(data,x,y) {

        //var date=data['query']['results']['quote']['Date'];
        var quoteArray = data['data'];
        console.log(quoteArray);
        var theLength = quoteArray.length; //get size of array

        var date = [];

        //convert date to milliseconds
        for (i=0; i< theLength; i++) {
            var myDate = new Date(quoteArray[i][0]);
            var dateyo = myDate.getTime(); //date in milliseconds to plot correctly with highCharts
            date.push([
                dateyo
            ]);
        }
        console.log(date[1][0]);
        //populate arrays for highchart
        var ohlc = [],
            volume = [];
        for (i=0; i<theLength; i++) {
            ohlc.push([
                date[i][0], //the date
                quoteArray[i][1], //open
                quoteArray[i][2], //high
                quoteArray[i][3], //low
                quoteArray[i][4] //close
            ]);

            volume.push([
                date[i][0], //date
                quoteArray[i][5] //Volume
            ])
        }

            // set the allowed units for data grouping
        var groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]];

        // create the chart
        $('#container3').highcharts('StockChart', {
            
            rangeSelector: {
                selected: 1
            },

            title: {
                text: ''+symbol3+' Historical'
            },

            yAxis: [{
                title: {
                    text: 'OHLC'
                },
                height: 200,
                lineWidth: 2
            }, {
                title: {
                    text: 'Volume'
                },
                top: 300,
                height: 100,
                offset: 0,
                lineWidth: 2
            }],
            
            series: [{
                type: 'candlestick',
                name: symbol3,
                data: ohlc,
                dataGrouping: {
                    units: groupingUnits
                }
            }, {
                type: 'column',
                name: 'Volume',
                data: volume,
                yAxis: 1,
                dataGrouping: {
                    units: groupingUnits
                }
            }]
        });
    });
});